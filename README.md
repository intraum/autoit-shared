# Наборы подключаемых фунций
Указаны только либо частично, либо полностью заимствованные.

## ArraysCompare.au3
Сравнение двух массивов, на основе [этого решения](https://www.autoitscript.com/forum/topic/164728-compare-2-arrays-with-3-arrays-as-a-result/?tab=comments#comment-1201986).

## ZLIB.au3
UDF [отсюда](https://www.autoitscript.com/forum/topic/128962-zlib-deflateinflategzip-udf/).

## _Zip.au3
UDF [отсюда](https://www.autoitscript.com/forum/topic/116565-zip-udf-zipfldrdll-library/).

## FileTimes.au3
Работа с атрибутами времени файлов, на основе [этого решения](https://www.autoitscript.com/forum/topic/19129-finding-the-latest-file/?tab=comments#comment-1347403).

## StringAux.au3
Дополнительные функции для операций со строками. Экранирование для регулярок -- [эта UDF](https://www.autoitscript.com/forum/topic/151918-_stringregexpmetacharacters-retain-special-characters-for-use-in-a-regular-expression/).

## FileAux.au3
Дополнительные функции для файловых операций. Проверка файла на занятость на основе [этого решения](https://www.autoitscript.com/forum/topic/53994-need-help-with-copy-verification/?do=findComment&comment=410020).

## GUIAux.au3
Дополнительные функции для операций с графическими интерфейсами.

## ExcelAux.au3
Дополнительные функции для операций с Excel.

## ArrayRemoveBlanks.au3
Удаление пустых элементов в одномерном массиве; удаление строк с пустыми элементами в указанном столбце двухмерного массива. Поддерживается очистка от всех пробелов перед проверкой.

## DTC.au3
UDF [отсюда](https://www.autoitscript.com/forum/topic/154684-date_time_convert-bugfix-version-27-may-15/).

## StringToArray2D.au3
На основе [этого решения](https://www.autoitscript.com/forum/topic/148947-multi-dimensional-arrays-stringsplit-function/?do=findComment&comment=1060391).

## ArrayWorkshop.au3
UDF [отсюда](https://www.autoitscript.com/forum/topic/180467-arrayworkshop/).

## JSON/Json.au3 и JSON/BinaryCall.au3
UDF [отсюда](https://www.autoitscript.com/forum/topic/148114-a-non-strict-json-udf-jsmn/).
